﻿namespace PathOfMapsClient {
    partial class ZanaModForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnRampage = new PathOfMapsClient.FutureButton();
            this.btnBloodlines = new PathOfMapsClient.FutureButton();
            this.btnTorment = new PathOfMapsClient.FutureButton();
            this.btnTempest = new PathOfMapsClient.FutureButton();
            this.btnDomination = new PathOfMapsClient.FutureButton();
            this.btnNoMod = new PathOfMapsClient.FutureButton();
            this.btnAmbush = new PathOfMapsClient.FutureButton();
            this.btnNemesis = new PathOfMapsClient.FutureButton();
            this.SuspendLayout();
            // 
            // btnRampage
            // 
            this.btnRampage.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnRampage.Location = new System.Drawing.Point(12, 12);
            this.btnRampage.Name = "btnRampage";
            this.btnRampage.Size = new System.Drawing.Size(106, 44);
            this.btnRampage.TabIndex = 0;
            this.btnRampage.Text = "Rampage (2c)";
            this.btnRampage.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnBloodlines
            // 
            this.btnBloodlines.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnBloodlines.Location = new System.Drawing.Point(12, 62);
            this.btnBloodlines.Name = "btnBloodlines";
            this.btnBloodlines.Size = new System.Drawing.Size(106, 44);
            this.btnBloodlines.TabIndex = 1;
            this.btnBloodlines.Text = "Bloodlines (3c)";
            this.btnBloodlines.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnTorment
            // 
            this.btnTorment.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnTorment.Location = new System.Drawing.Point(12, 112);
            this.btnTorment.Name = "btnTorment";
            this.btnTorment.Size = new System.Drawing.Size(106, 44);
            this.btnTorment.TabIndex = 2;
            this.btnTorment.Text = "Torment (4c)";
            this.btnTorment.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnTempest
            // 
            this.btnTempest.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnTempest.Location = new System.Drawing.Point(12, 162);
            this.btnTempest.Name = "btnTempest";
            this.btnTempest.Size = new System.Drawing.Size(106, 44);
            this.btnTempest.TabIndex = 3;
            this.btnTempest.Text = "Tempest (4c)";
            this.btnTempest.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnDomination
            // 
            this.btnDomination.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnDomination.Location = new System.Drawing.Point(124, 12);
            this.btnDomination.Name = "btnDomination";
            this.btnDomination.Size = new System.Drawing.Size(106, 44);
            this.btnDomination.TabIndex = 4;
            this.btnDomination.Text = "Domination (5c)";
            this.btnDomination.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnNoMod
            // 
            this.btnNoMod.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnNoMod.Location = new System.Drawing.Point(12, 212);
            this.btnNoMod.Name = "btnNoMod";
            this.btnNoMod.Size = new System.Drawing.Size(218, 44);
            this.btnNoMod.TabIndex = 5;
            this.btnNoMod.Text = "None";
            this.btnNoMod.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnAmbush
            // 
            this.btnAmbush.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnAmbush.Location = new System.Drawing.Point(124, 62);
            this.btnAmbush.Name = "btnAmbush";
            this.btnAmbush.Size = new System.Drawing.Size(106, 44);
            this.btnAmbush.TabIndex = 6;
            this.btnAmbush.Text = "Ambush (6c)";
            this.btnAmbush.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnNemesis
            // 
            this.btnNemesis.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnNemesis.Location = new System.Drawing.Point(124, 112);
            this.btnNemesis.Name = "btnNemesis";
            this.btnNemesis.Size = new System.Drawing.Size(106, 44);
            this.btnNemesis.TabIndex = 7;
            this.btnNemesis.Text = "Nemesis (8c)";
            this.btnNemesis.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // ZanaModForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(247, 269);
            this.Controls.Add(this.btnNemesis);
            this.Controls.Add(this.btnAmbush);
            this.Controls.Add(this.btnNoMod);
            this.Controls.Add(this.btnDomination);
            this.Controls.Add(this.btnTempest);
            this.Controls.Add(this.btnTorment);
            this.Controls.Add(this.btnBloodlines);
            this.Controls.Add(this.btnRampage);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ZanaModForm";
            this.Text = "Add Zana mod";
            this.ResumeLayout(false);

        }

        #endregion

        private FutureButton btnRampage;
        private FutureButton btnBloodlines;
        private FutureButton btnTorment;
        private FutureButton btnTempest;
        private FutureButton btnDomination;
        private FutureButton btnNoMod;
        private FutureButton btnAmbush;
        private FutureButton btnNemesis;
    }
}