﻿namespace PathOfMapsClient {
    partial class FoundMasterForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.picHaku = new System.Windows.Forms.PictureBox();
            this.picElreon = new System.Windows.Forms.PictureBox();
            this.picCatarina = new System.Windows.Forms.PictureBox();
            this.picTora = new System.Windows.Forms.PictureBox();
            this.picVorici = new System.Windows.Forms.PictureBox();
            this.picVagan = new System.Windows.Forms.PictureBox();
            this.picZana = new System.Windows.Forms.PictureBox();
            this.btnNoMaster = new PathOfMapsClient.FutureButton();
            ((System.ComponentModel.ISupportInitialize)(this.picHaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picElreon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCatarina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVorici)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVagan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZana)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(175)))), ((int)(((byte)(244)))));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(483, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Which master have you found?";
            // 
            // picHaku
            // 
            this.picHaku.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picHaku.Image = global::PathOfMapsClient.Properties.Resources.haku;
            this.picHaku.Location = new System.Drawing.Point(19, 57);
            this.picHaku.Name = "picHaku";
            this.picHaku.Size = new System.Drawing.Size(100, 147);
            this.picHaku.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picHaku.TabIndex = 1;
            this.picHaku.TabStop = false;
            this.picHaku.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // picElreon
            // 
            this.picElreon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picElreon.Image = global::PathOfMapsClient.Properties.Resources.elreon_84924;
            this.picElreon.Location = new System.Drawing.Point(125, 57);
            this.picElreon.Name = "picElreon";
            this.picElreon.Size = new System.Drawing.Size(100, 147);
            this.picElreon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picElreon.TabIndex = 2;
            this.picElreon.TabStop = false;
            this.picElreon.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // picCatarina
            // 
            this.picCatarina.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picCatarina.Image = global::PathOfMapsClient.Properties.Resources.catarina_89922;
            this.picCatarina.Location = new System.Drawing.Point(231, 57);
            this.picCatarina.Name = "picCatarina";
            this.picCatarina.Size = new System.Drawing.Size(128, 147);
            this.picCatarina.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picCatarina.TabIndex = 3;
            this.picCatarina.TabStop = false;
            this.picCatarina.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // picTora
            // 
            this.picTora.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picTora.Image = global::PathOfMapsClient.Properties.Resources.tora_38929;
            this.picTora.Location = new System.Drawing.Point(365, 57);
            this.picTora.Name = "picTora";
            this.picTora.Size = new System.Drawing.Size(128, 147);
            this.picTora.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picTora.TabIndex = 4;
            this.picTora.TabStop = false;
            this.picTora.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // picVorici
            // 
            this.picVorici.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picVorici.Image = global::PathOfMapsClient.Properties.Resources.vorici;
            this.picVorici.Location = new System.Drawing.Point(19, 210);
            this.picVorici.Name = "picVorici";
            this.picVorici.Size = new System.Drawing.Size(100, 147);
            this.picVorici.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picVorici.TabIndex = 5;
            this.picVorici.TabStop = false;
            this.picVorici.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // picVagan
            // 
            this.picVagan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picVagan.Image = global::PathOfMapsClient.Properties.Resources.vagan_394929;
            this.picVagan.Location = new System.Drawing.Point(125, 210);
            this.picVagan.Name = "picVagan";
            this.picVagan.Size = new System.Drawing.Size(100, 147);
            this.picVagan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picVagan.TabIndex = 6;
            this.picVagan.TabStop = false;
            this.picVagan.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // picZana
            // 
            this.picZana.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picZana.Image = global::PathOfMapsClient.Properties.Resources.zana_2399;
            this.picZana.Location = new System.Drawing.Point(231, 210);
            this.picZana.Name = "picZana";
            this.picZana.Size = new System.Drawing.Size(128, 147);
            this.picZana.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picZana.TabIndex = 7;
            this.picZana.TabStop = false;
            this.picZana.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // btnNoMaster
            // 
            this.btnNoMaster.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNoMaster.Font = new System.Drawing.Font("Verdana", 22F);
            this.btnNoMaster.Location = new System.Drawing.Point(365, 210);
            this.btnNoMaster.Name = "btnNoMaster";
            this.btnNoMaster.Size = new System.Drawing.Size(128, 147);
            this.btnNoMaster.TabIndex = 8;
            this.btnNoMaster.Text = "None";
            this.btnNoMaster.Click += new System.EventHandler(this.btnMasterClick);
            // 
            // FoundMasterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(507, 370);
            this.Controls.Add(this.btnNoMaster);
            this.Controls.Add(this.picZana);
            this.Controls.Add(this.picVagan);
            this.Controls.Add(this.picVorici);
            this.Controls.Add(this.picTora);
            this.Controls.Add(this.picCatarina);
            this.Controls.Add(this.picElreon);
            this.Controls.Add(this.picHaku);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FoundMasterForm";
            this.Text = "Found a master";
            ((System.ComponentModel.ISupportInitialize)(this.picHaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picElreon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCatarina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVorici)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVagan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZana)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picHaku;
        private System.Windows.Forms.PictureBox picElreon;
        private System.Windows.Forms.PictureBox picCatarina;
        private System.Windows.Forms.PictureBox picTora;
        private System.Windows.Forms.PictureBox picVorici;
        private System.Windows.Forms.PictureBox picVagan;
        private System.Windows.Forms.PictureBox picZana;
        private FutureButton btnNoMaster;
    }
}